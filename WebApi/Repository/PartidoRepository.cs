﻿using Repository.Mongo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Repository
{
    public class PartidoRepository : Repository<Partido>
    {
        public PartidoRepository(string connectionString) : base(connectionString) { }
    }
}
