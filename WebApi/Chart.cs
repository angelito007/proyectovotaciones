﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi
{
    public class Chart
    {
        public string Label { get; set; }
        public int value { get; set; }
        public string color { get; set; }
    }
}
