﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApi.Repository;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VotosController : ControllerBase
    {
        ParticipanteRepository repo = new ParticipanteRepository("mongodb://usuario:usuario123@ds233288.mlab.com:33288/elecciones?retryWrites=false");
        PartidoRepository repo2 = new PartidoRepository("mongodb://usuario:usuario123@ds233288.mlab.com:33288/elecciones?retryWrites=false");
        // GET: api/Votos



        private static readonly string[] Summaries = new[]
   {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<VotosController> _logger;

        public VotosController(ILogger<VotosController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }

        [HttpPost]
        public bool Post([FromBody] Voto voto)
        {
            try
            {
                var participantes = repo.Get(voto.Participante);
                if (!participantes.voto)
                {
                    participantes.voto = true;
                    repo.Replace(participantes);
                    var partidos = repo2.Get(voto.Partido);
                    partidos.Votos += 1;
                    repo2.Replace(partidos);
                    Chart chart = new Chart();
                    chart.Label = $"{partidos.Nombre}  {partidos.Sigla}";
                    chart.color = partidos.Color;
                    chart.value = partidos.Votos;
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
           
        }
        // PUT: api/Votos/5



    }
}
