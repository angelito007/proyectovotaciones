﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi
{
    public class Voto
    {
        public string Participante { get; set; }
        public string Partido { get; set; }
    }
}
