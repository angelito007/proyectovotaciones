'use strict'
var Votante= require('../Models/ciudadano.model')


function createUser (req,res){
    var votante= new Votante;
    var data = req.body;

    console.log(data);

    votante.name = data.name;
    votante.lastname= data.lastname;
    votante.ci=data.ci;
    votante.votacion.estado= data.votacion.estado;
    votante.votacion.partido = data.votacion.partido;

    votante.save((error,usuarioreg)=>{
        if(error)
        {
            console.log("error");
        }
        else{
            res.status(200).send(usuarioreg);
            //res.send({message:"dato guardado"});
        }
    }); 

}

function readUser(req,res)
{    
    Votante.find((error,data)=>{
          if(error){
              console.log("error")
              res.status(500).send("error, datos vacios");
          }
          else{
              res.status(200).send(data);
          }
      })

    //res.status(200).send("test de ingreso a api");

}

function updateUser(req,res){
    

    var data= req.body;
  
    Usuario.updateOne( {_id:req.params.id} ,data, (error,usuarionew) =>{
        console.log(req.params.id);

        if(error)
        {
            res.status(500).send({message:"error al actualizar"});
            console.log(error);
        }
        else{
            res.status(200).send({dataupdate:usuarionew ,message:"datos guardados"});
        }
    });
    
}

function deleteUser(req,res){
    var data = req.body;

    Usuario.deleteOne({_id:req.params.id}, (error,usuarioborrado) =>{
        if(error){
            res.status(500).send({message:"error al borrar"});
            console.log(error);
        }
        else{
            res.status(200).send({datadelete:usuarioborrado, message:"datos borrados"});
        }
    });
}

module.exports={
    createUser,
    readUser,
    updateUser,
    deleteUser
}