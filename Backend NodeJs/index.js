'use strict'
const app = require('./app.js')
const mongoose = require('mongoose');

//const http = require('http');
//.Server(app)

mongoose.connect('mongodb://localhost/votaciones', {useNewUrlParser: true},(error)=>{
    if(error)
    {
        console.log("error", error);
    }
    else{
        console.log("conexion a la base de datos");
        app.listen(3000,()=>{
            console.log("servidor abierto en puerto 3000");
        })
        // http.listen(3000,()=>{
        //     console.log("servidor abierto en puerto 3000".green);
        // })
    }
});