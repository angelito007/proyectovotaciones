const express= require ('express');

const bodyParser = require('body-parser');

const usuarioRouter = require('./routes/votante.route');

const app = express();


app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())


app.use('/api/votantes',usuarioRouter)



module.exports = app;