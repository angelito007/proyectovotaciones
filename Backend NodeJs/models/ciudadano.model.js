'use strict'

const mongoose = require('mongoose');

const schema = mongoose.Schema;

var Votante = new schema({
    name:String,
    lastname:String,   
    ci:String,
    votacion:{
        estado: Boolean,
        partido:String,        
    }
});

module.exports= mongoose.model('Votante',Votante);