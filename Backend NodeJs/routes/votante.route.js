const express = require('express');

const router = express.Router();

const votanteController = require('../Controllers/votar.controller');

router.get('/',votanteController.readUser);
router.post('/',votanteController.createUser);
router.put('/:id',votanteController.updateUser);
router.delete('/:id',votanteController.deleteUser);

module.exports=router;
