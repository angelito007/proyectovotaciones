﻿using Repository.Mongo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication7.Models
{
    public class Usuario : Entity
    {
        public string nombre { get; set; }
        public string apellido { get; set; }
    }
}
