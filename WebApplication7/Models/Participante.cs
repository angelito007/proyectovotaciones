﻿using Repository.Mongo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication7.Models
{
    public class Participante: Entity
    {
      
        public int Nro { get; set; }
        public string Departamento { get; set; }
        public string Municipio { get; set; }
        public string Nombres { get; set; }
        public string NroDocumento { get; set; }
        public bool voto { get; set; }
        public string tipo { get; set; }
    }
}
