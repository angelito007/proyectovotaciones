﻿using Repository.Mongo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication7.Models
{
    public class Partido : Entity
    {
        public string Nombre { get; set; }
        public string Sigla { get; set; }
        public string Postulante { get; set; }
        public string Color { get; set; }

        public int Votos { get; set; }
    }
}
