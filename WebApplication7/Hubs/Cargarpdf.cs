﻿using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using WebApplication7.Models;
using WebApplication7.Repositorios;

namespace WebApplication7.Hubs
{
    public class Cargarpdf:Hub
    {
        ParticipanteRepository repo = new ParticipanteRepository("mongodb://usuario:usuario123@ds233288.mlab.com:33288/elecciones?retryWrites=false");

        public async Task Cargar()
        {
            if (repo.EstimatedCount()<1)
            {

                string documento = pdfText(@"inhabilitados_cochabamba.pdf");
                await Clients.Caller.SendAsync("Extaendo", true);
                List<Participante> participantes = TextToObjet(documento);
                await Clients.Caller.SendAsync("cargandodb", true);
                foreach (var participante in participantes)
                {
                    repo.Insert(participante);
                 
                }
                await Clients.Caller.SendAsync("terminado", true);
            }
            else
            {
                await Clients.Caller.SendAsync("terminado", true);
            }

        }
        
        public static string pdfText(string path)
        {
            PdfReader reader = new PdfReader(path);

            string text = string.Empty;
            for (int page = 1; page <= reader.NumberOfPages; page++)
            {
                text += PdfTextExtractor.GetTextFromPage(reader, page);
                text += "|\n";
            }
            reader.Close();
            return text;
        }

        public List<Participante> TextToObjet(string text)
        {

            var urls = text.Split(new[] { "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries) as string[];
            List<Participante> de = new List<Participante>();
            for (int i = 4; i <= urls.Length; i++)
            {
                try
                {
                    Participante participante = new Participante();


                    string[] partes = Regex.Split(urls[i], @"\W+");
                    participante.Nro = int.Parse(partes[0]);
                    participante.Departamento = partes[1];
                    participante.Municipio = partes[2];
                    participante.tipo = "comun";
                    int split = 0;
                    for (int f = 3; f < partes.Length; f++)
                    {
                        int number;
                        if (int.TryParse(partes[f], out number) || partes[f].Contains("-"))
                        {
                            split = f;
                        }
                    }
                    participante.NroDocumento = partes[split];
                    for (int f = 3; f < split; f++)
                    {
                        participante.Nombres += $"{partes[f]} ";
                    }
                    participante.Nombres.Replace("|", "");
                    de.Add(participante);
                }
                catch (Exception ex)
                {

                }

            }
            return de;
        }

    }
}
