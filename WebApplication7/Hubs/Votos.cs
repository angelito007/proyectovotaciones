﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication7.Models;
using WebApplication7.Repositorios;

namespace WebApplication7.Hubs
{
    public class Votos:Hub
    {
        ParticipanteRepository repo = new ParticipanteRepository("mongodb://usuario:usuario123@ds233288.mlab.com:33288/elecciones?retryWrites=false");
        PartidoRepository repo2 = new PartidoRepository("mongodb://usuario:usuario123@ds233288.mlab.com:33288/elecciones?retryWrites=false");
        public async Task Votar(string participante,string partido)
        {
            var participantes = repo.Get(participante);
            participantes.voto = true;
            repo.Replace(participantes);
            var partidos = repo2.Get(partido);
            partidos.Votos += 1;
            repo2.Replace(partidos);

            await Clients.Caller.SendAsync("voto", true);
            Chart chart = new Chart();
            chart.Label = $"{partidos.Nombre}  {partidos.Sigla}";
            chart.color = partidos.Color;
            chart.value = partidos.Votos;
            await Clients.All.SendAsync("votos", chart);


        }
    }
}
