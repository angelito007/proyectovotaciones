﻿using Repository.Mongo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication7.Models;

namespace WebApplication7.Repositorios
{
    public class UsuarioRepository : Repository<Usuario>
    {
        public UsuarioRepository(string connectionString) : base(connectionString) { }

        //custom method
        public Usuario FindbyUsername(string nombre)
        {
            return First(i => i.nombre == nombre);
        }

        //custom method2
        public void UpdatePassword(Usuario item, string apellido)
        {
            Update(item, i => i.apellido, apellido);
        }
    }
}
