﻿using Repository.Mongo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication7.Models;

namespace WebApplication7.Repositorios
{
    public class PartidoRepository: Repository<Partido>
    {
        public PartidoRepository(string connectionString) : base(connectionString) { }
    }
}
