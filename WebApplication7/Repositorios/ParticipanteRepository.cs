﻿using Repository.Mongo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication7.Models;

namespace WebApplication7.Repositorios
{
    public class ParticipanteRepository : Repository<Participante>
    {
        public ParticipanteRepository(string connectionString) : base(connectionString) { }

        public Participante FindByCodigo(string codigo)
        {
            return First(i => i.NroDocumento == codigo);
        }
    }
}
