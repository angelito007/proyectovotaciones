﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Wangkanai.Detection;
using WebApplication7.Models;
using WebApplication7.Repositorios;

namespace WebApplication7.Controllers
{
    public class VotacionesController : Controller
    {
        PartidoRepository repo2 = new PartidoRepository("mongodb://usuario:usuario123@ds233288.mlab.com:33288/elecciones?retryWrites=false");
        ParticipanteRepository repo = new ParticipanteRepository("mongodb://usuario:usuario123@ds233288.mlab.com:33288/elecciones?retryWrites=false");


        private readonly ILogger<VotacionesController> _logger;
        private readonly IDevice device;


        public VotacionesController(ILogger<VotacionesController> logger, IDeviceResolver deviceResolver)
        {
            this.device = deviceResolver.Device;
            _logger = logger;
        }
        // GET: Votaciones

        public ActionResult Index(string id)
        {
            if (device.Type == DeviceType.Desktop)
            {
                try
                {
                    var participante = repo.Get(id);
                    if (participante.tipo == "jurado")
                    {
                        return View();
                    }
                    else
                    {
                        return RedirectToAction("Entrar");
                    }

                }
                catch (Exception)
                {

                    return RedirectToAction("Entrar");
                }
            }
            else
            {
                return RedirectToAction("Index", "Mobile", new { id = id });
            }
               
           
        }



        // GET: Votaciones/Create
        public ActionResult Entrar()
        {

            if (device.Type == DeviceType.Desktop)
            {
                //some logic
                return View();
            }
            else
            {
                //some logic
                return RedirectToAction("Entrar", "Mobile");
            }
        }

       public ActionResult Partidos(string id)
        {
            if (device.Type == DeviceType.Desktop)
            {
                try
                {
                    var partidos = repo2.FindAll();
                    var participante = repo.Get(id);
                    if (participante != null)
                    {
                        if (participante.voto)
                        {
                            return RedirectToAction("Index");
                        }
                        ViewData["participante"] = id;
                        return View(partidos);
                    }
                    else
                    {
                        return RedirectToAction("Entrar");
                    }
                }
                catch (Exception)
                {

                    return RedirectToAction("Entrar");
                }
            }
            else
            {
                return RedirectToAction("Partidos", "Mobile", new { id = id });
            }
                
           
        }
       
        // POST: Votaciones/Create

        public ActionResult Validar(string codigo)
        {
            try
            {
                var usuario = repo.FindByCodigo(codigo);
                if (usuario != null)
                {
                    if (usuario.voto)
                    {
                        if (usuario.tipo == "jurado")
                        {
                            return RedirectToAction("Index", new { id = usuario.Id });
                        }
                        else
                        {
                            ViewData["error"] = "Ya botaste ";
                            return View("Entrar");
                        }
                    }
                    return RedirectToAction("Partidos", new { id = usuario.Id });
                }
                else
                {
                    ViewData["error"] = "Participante no registrado";
                    return View("Entrar");
                }
              
            }
            catch
            {
                ViewData["error"] = "Participante no registrado";
                return View("Entrar");
            }
        }

       
    }
}