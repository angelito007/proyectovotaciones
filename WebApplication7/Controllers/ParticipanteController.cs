﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication7.Models;
using WebApplication7.Repositorios;

namespace WebApplication7.Controllers
{
    public class ParticipanteController : Controller
    {
        ParticipanteRepository repo = new ParticipanteRepository("mongodb://usuario:usuario123@ds233288.mlab.com:33288/elecciones?retryWrites=false");
        // GET: Usuario
        public ActionResult Index()
        {
            var usuarios = repo.Find(p=>p.tipo== "jurado");
            return View(usuarios);
        }

        // GET: Participante/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Participante/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Participante/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Participante collection)
        {
            try
            {
                collection.tipo = "jurado";
                // TODO: Add insert logic here
                repo.Insert(collection);
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                return View();
            }
        }
        // GET: Participante/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Participante/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Participante/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Participante/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}