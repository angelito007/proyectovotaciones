﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication7.Models;
using WebApplication7.Repositorios;

namespace WebApplication7.Controllers
{
    public class UsuarioController : Controller
    {
        UsuarioRepository repo = new UsuarioRepository("mongodb://usuario:usuario123@ds233288.mlab.com:33288/elecciones?retryWrites=false");
        // GET: Usuario
        public ActionResult Index()
        {
            var usuarios = repo.FindAll();
            return View(usuarios);
        }

        // GET: Usuario/Details/5
        public ActionResult Details(string id)
        {
            var usuario=  repo.Get(id);
            return View(usuario);
        }

        // GET: Usuario/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Usuario/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Usuario collection)
        {
            try
            {
                // TODO: Add insert logic here
                repo.Insert(collection);
                return RedirectToAction(nameof(Index));
            }
            catch(Exception e)
            {
                return View();
            }
        }

        // GET: Usuario/Edit/5
        public ActionResult Edit(string id)
        {
            var usuario = repo.Get(id);
            return View(usuario);
        }

        // POST: Usuario/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(string id, Usuario collection)
        {
            try
            {
                collection.Id = id;
                // TODO: Add update logic here
                repo.Replace(collection);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Usuario/Delete/5
        public ActionResult Delete(string id)
        {
            var usuario = repo.Get(id);

            return View(usuario);
        }

        // POST: Usuario/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(string id, Usuario collection)
        {
            try
            {
                collection.Id = id;
                // TODO: Add delete logic here
                repo.Delete(collection);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}