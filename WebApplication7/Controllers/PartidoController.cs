﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication7.Models;
using WebApplication7.Repositorios;

namespace WebApplication7.Controllers
{
    public class PartidoController : Controller
    {
        PartidoRepository repo = new PartidoRepository("mongodb://usuario:usuario123@ds233288.mlab.com:33288/elecciones?retryWrites=false");
        // GET: Partido
        public ActionResult Index()
        {
            var partidos = repo.FindAll();
            return View(partidos);
        }

        public List<Chart> Resultados()
        {
            List<Chart> datos = new List<Chart>();
            var partidos = repo.FindAll();
            foreach (var partido in partidos)
            {
                datos.Add(new Chart { Label = $"{partido.Nombre}  {partido.Sigla}", value = partido.Votos, color = partido.Color });
            }
            return datos;
        }

        // GET: Partido/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Partido/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Partido/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Partido collection)
        {
            Random rnd = new Random();
            try
            {
                int votos = rnd.Next(30, 50);
                collection.Votos = votos;
                // TODO: Add insert logic here
                repo.Insert(collection);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Partido/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Partido/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Partido/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Partido/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}