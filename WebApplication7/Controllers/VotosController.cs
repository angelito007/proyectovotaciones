﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication7.Models;
using WebApplication7.Repositorios;

namespace WebApplication7.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VotosController : ControllerBase
    {
        ParticipanteRepository repo = new ParticipanteRepository("mongodb://usuario:usuario123@ds233288.mlab.com:33288/elecciones?retryWrites=false");
        PartidoRepository repo2 = new PartidoRepository("mongodb://usuario:usuario123@ds233288.mlab.com:33288/elecciones?retryWrites=false");
        // GET: api/Votos
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Votos/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Votos
        [HttpPost]
        public bool Post([FromBody] string participante, string partido)
        {
            var participantes = repo.Get(participante);
            if (!participantes.voto)
            {
                participantes.voto = true;
                repo.Replace(participantes);
                var partidos = repo2.Get(partido);
                partidos.Votos += 1;
                repo2.Replace(partidos);
                Chart chart = new Chart();
                chart.Label = $"{partidos.Nombre}  {partidos.Sigla}";
                chart.color = partidos.Color;
                chart.value = partidos.Votos;
                return true;
            }
            else
            {
                return false;
            }
           
        }

        // PUT: api/Votos/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
