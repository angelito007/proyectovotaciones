﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Wangkanai.Detection;
using WebApplication7.Repositorios;

namespace WebApplication7.Controllers
{
    
    public class MobileController : Controller
    {
        PartidoRepository repo2 = new PartidoRepository("mongodb://usuario:usuario123@ds233288.mlab.com:33288/elecciones?retryWrites=false");
        ParticipanteRepository repo = new ParticipanteRepository("mongodb://usuario:usuario123@ds233288.mlab.com:33288/elecciones?retryWrites=false");
        private readonly ILogger<HomeController> _logger;
        private readonly IDevice device;


        public MobileController(ILogger<HomeController> logger, IDeviceResolver deviceResolver)
        {
            this.device = deviceResolver.Device;
            _logger = logger;
        }
        // GET: Mobile
        public ActionResult Index(string id)
        {
            if (device.Type == DeviceType.Mobile)
            {
                //some logic
                return View();
            }
            else
            {
                //some logic
                return RedirectToAction("Index", "Home");
            }
        }

       
        public ActionResult Partidos(string id)
        {
            if (device.Type == DeviceType.Mobile)
            {
                try
                {
                    var partidos = repo2.FindAll();
                    var participante = repo.Get(id);
                    if (participante != null)
                    {
                        if (participante.voto)
                        {
                            return RedirectToAction("Entrar");
                        }
                        ViewData["participante"] = id;
                        return View(partidos);
                    }
                    else
                    {
                        return RedirectToAction("Entrar");
                    }
                }
                catch (Exception)
                {

                    return RedirectToAction("Entrar");
                }

            }
            else
            {
                //some logic
                return RedirectToAction("Partidos", "Votaciones", new { id = id });
            }
          
        }

        public ActionResult Entrar()
        {
            if (device.Type == DeviceType.Mobile)
            {
                //some logic
                return View();
            }
            else
            {
                //some logic
                return RedirectToAction("Entrar", "Votaciones");
            }
        }

        public ActionResult Resultados(string id)
        {
            if (device.Type == DeviceType.Mobile)
            {
                //some logic
                try
                {
                    var participante = repo.Get(id);
                    if (participante.tipo == "jurado")
                    {
                        return View();
                    }
                    else
                    {
                        return RedirectToAction("Entrar");
                    }

                }
                catch (Exception)
                {

                    return RedirectToAction("Entrar");
                }
            }
            else
            {
                //some logic
                return RedirectToAction("Index", "Votaciones", new { id = id});
            }
        }
        public ActionResult Validar(string codigo)
        {
            try
            {
                var usuario = repo.FindByCodigo(codigo);
                if (usuario != null)
                {
                    if (usuario.voto)
                    {
                        if (usuario.tipo == "jurado")
                        {
                            return RedirectToAction("Resultados", new { id = usuario.Id });
                        }
                        else
                        {
                            ViewData["error"] = "Ya botaste ";
                            return View("Entrar");
                        }
                    }
                    return RedirectToAction("Partidos", new { id = usuario.Id });
                }
                else
                {
                    ViewData["error"] = "Participante no registrado";
                    return View("Entrar");
                }

            }
            catch
            {
                ViewData["error"] = "Participante no registrado";
                return View("Entrar");
            }
        }
       
    }
}